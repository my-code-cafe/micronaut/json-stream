package com.example.stream;

import io.micronaut.runtime.EmbeddedApplication;
import io.micronaut.test.extensions.junit5.annotation.MicronautTest;
import jakarta.inject.Inject;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

@MicronautTest
class StreamTest {

    @Inject
    EmbeddedApplication<?> application;

    @Test
    void testItWorks() {
        Assertions.assertTrue(application.isRunning());
    }

    @Test
    void formattedWorks() {
        Assertions.assertEquals("Welcome, Guest!", "Welcome, %s!".formatted("Guest"));
    }

    @Test
    void patternMatching() {
        Assertions.assertEquals(
                "Employee name is Code Emp",
                eval(new Employee(10, "Code Emp"))
        );

        Assertions.assertEquals(
                "Vendor is Consulting Co.",
                eval(new Contractor(20, "Code Const", "Consulting Co."))
        );
    }

    String eval(Person person) {
        return switch (person) {
            case Employee(int id, String name) -> "Employee name is %s".formatted(name);
            case Contractor(int id, String name, String vendor) -> "Vendor is %s".formatted(vendor);
        };
    }
}