package com.example.stream;

import io.micronaut.http.HttpRequest;
import io.micronaut.http.client.StreamingHttpClient;
import io.micronaut.http.client.annotation.Client;
import io.micronaut.test.extensions.junit5.annotation.MicronautTest;
import jakarta.inject.Inject;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import reactor.core.publisher.Flux;
import reactor.util.function.Tuple2;
import reactor.util.function.Tuples;

import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

@MicronautTest
class StreamControllerTest {

    @Inject
    @Client("/")
    StreamingHttpClient streamingHttpClient;

    @Test
    void testStream() {
        AtomicLong currentTime = new AtomicLong(System.currentTimeMillis());

        List<Tuple2<Long, Employee>> responses = Flux.from(streamingHttpClient.jsonStream(HttpRequest.GET("/stream"), Employee.class))
                .map(emp -> Tuples.of(System.currentTimeMillis() - currentTime.getAndSet(System.currentTimeMillis()), emp))
                .collectList()
                .block();

        assert responses != null;
        Assertions.assertEquals(10, responses.size());
        responses.forEach(tuple -> {
            System.out.println("Duration: " + tuple.getT1());
            Assertions.assertTrue(tuple.getT1() > 800 && tuple.getT1() < 2000);
        });
    }
}