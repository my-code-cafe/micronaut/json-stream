package com.example.stream;

public sealed interface Person
        permits Employee, Contractor
{
    String name();
}
