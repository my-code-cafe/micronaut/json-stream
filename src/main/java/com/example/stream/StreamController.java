package com.example.stream;

import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import reactor.core.publisher.Flux;

import java.time.Duration;

@Controller
public class StreamController {

    @Get(value = "/stream", produces = MediaType.APPLICATION_JSON_STREAM)
    public Flux<Employee> stream() {
        return Flux.range(1, 10)
                .delayElements(Duration.ofMillis(1000))
                .map(i -> new Employee(i,"Emp-" + i));
    }
}
