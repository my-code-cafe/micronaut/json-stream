package com.example.stream;

public record Employee(int id, String name) implements Person {
}
