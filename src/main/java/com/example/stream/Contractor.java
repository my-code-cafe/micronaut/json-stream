package com.example.stream;

public record Contractor(int id, String name, String vendor) implements Person {
}
